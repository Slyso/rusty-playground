use rand::random;
use std::time::Instant;
use rand::distributions::{Distribution, Standard};

const ELEMENT_COUNT: usize = 500;

fn main() {
    let mut numbers: Vec<i8> = get_random_vec(ELEMENT_COUNT);

    let before = Instant::now();
    numbers.bubble_sort();

    assert!(numbers.is_sorted());
    println!("Bubble sort with {} elements took {:?}", ELEMENT_COUNT, before.elapsed());
}

fn get_random_vec<T>(size: usize) -> Vec<T> where Standard: Distribution<T> {
    let mut vec: Vec<T> = Vec::with_capacity(size);
    for _ in 0..size {
        vec.push(random());
    }

    vec
}

trait Sort {
    fn bubble_sort(&mut self);
}

impl<T: Ord + Copy> Sort for [T] {
    fn bubble_sort(&mut self) {
        for count_sorted in 0..self.len() - 1 {
            for i in 1..self.len() - count_sorted {
                if self[i - 1] > self[i] {
                    let previous = self[i - 1];
                    self[i - 1] = self[i];
                    self[i] = previous;
                }
            }
        }
    }
}
