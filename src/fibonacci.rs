use std::env;

fn main() {
    let input = env::args().nth(1).expect("Please specify one argument");
    let i = input.trim().parse::<usize>().expect("Positive integer argument expected");
    println!("{}", fibonacci_iter(i));
}

// a b
//   a b
// 0 1 1 2 3 5 8 13 21 34 55 ...
// 0 1 2 3 4 5 6 7  8  9  10
fn fibonacci(i: usize) -> usize {
    match i {
        0 => 0,
        1 => 1,
        n => fibonacci(n - 1).checked_add(fibonacci(n - 2)).unwrap(),
    }
}

fn fibonacci_iter(i: usize) -> usize {
    if i == 0 {
        return 0;
    }

    let mut a = 0usize;
    let mut b = 1usize;
    let mut tmp;

    for _ in 1..i {
        tmp = a;
        a = b;
        b = tmp.checked_add(b).expect("Overflowed");
    }

    b
}

#[cfg(test)]
mod test {
    use crate::{fibonacci, fibonacci_iter};

    #[test]
    fn test_fibonacci() {
        assert_eq!(fibonacci(0), 0);
        assert_eq!(fibonacci(1), 1);
        assert_eq!(fibonacci(2), 1);
        assert_eq!(fibonacci(3), 2);
        assert_eq!(fibonacci(4), 3);
        assert_eq!(fibonacci(20), 6765);
    }

    #[test]
    fn test_fibonacci_iter() {
        assert_eq!(fibonacci_iter(0), 0);
        assert_eq!(fibonacci_iter(1), 1);
        assert_eq!(fibonacci_iter(2), 1);
        assert_eq!(fibonacci_iter(3), 2);
        assert_eq!(fibonacci_iter(4), 3);
        assert_eq!(fibonacci_iter(20), 6765);
    }
}
